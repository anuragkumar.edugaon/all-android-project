package com.example.firebaseauth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        val HomeFragment = HomeFragment()
        val MessageFragment = MessageFragment()
        val profileFragment = ProfileFragment()

        setCurrentFragment(HomeFragment)

        val button = findViewById<BottomNavigationView>(R.id.buttom)
        button.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> setCurrentFragment(HomeFragment)
                R.id.Notification -> setCurrentFragment(MessageFragment)
                R.id.profile -> setCurrentFragment(profileFragment)
            }
            true
        }
    }
    private fun setCurrentFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }}


