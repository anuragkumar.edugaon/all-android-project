package com.example.tablayout

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.Fragment11
import com.example.whatsapp.Fragment2
import com.example.whatsapp.Fragment3

class TabsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(poasition: Int): CharSequence? {
        when (poasition) {
            0 -> {
                return "CHATS"
            }
            1 -> {
                return " STATUS "
            }
            2 -> {
                return " CALLS "
            }

        }
        return super.getPageTitle(poasition)
    }

    override fun getItem(poasition: Int): Fragment {

        return when (poasition) {
            0 -> {
                Fragment11()
            }
            1 -> {
                Fragment2()
            }

            else -> {
                Fragment3()
            }

        }

    }
}