package com.example.whatsapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CursorAdapter
import android.widget.ListView
import com.example.datalist
import com.example.tablayout.CustomAdaptor


class Fragment3 : Fragment() {
val customListModel=datalist.names

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val custom= inflater.inflate(R.layout.fragment_3, container, false)
        val custlistview=custom.findViewById<ListView>(R.id.fragment3)
        custlistview.adapter=activity?.let { CustomAdaptor(customListModel,it) }

        return custom
    }

}