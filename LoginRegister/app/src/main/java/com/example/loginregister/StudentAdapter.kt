package com.example.loginregister

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class CustomListAdapter (val studentDetailsArray:ArrayList<StudentAdapter>,val context: Context):BaseAdapter(){
    override fun getCount(): Int {
        return studentDetailsArray.size
    }

    override fun getItem(p0: Int): Any {
        return arrayList[p0]

    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView=LayoutInflater.from(context).inflate(R.layout.list_item,parent,false)
        val name=rowView.findViewById<TextView>(R.id.personName)
        val profileImage=rowView.findViewById<ImageView>(R.id.profile_pic)
        name.text=StudentAdapter[position].name

    }
    }
