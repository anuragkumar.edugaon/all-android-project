package Api

import android.database.Observable

interface userapi {
    interface UserApi {
        @GET("users")
        fun getUsers(): Observable<List<Testing>>

        companion object Factory{
            fun create():UserApi{
                val retrofit= Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://demo5896499.mockable.io")
                    .build()
                return retrofit.create(UserApi::class.java)
            }
        }

    }
}