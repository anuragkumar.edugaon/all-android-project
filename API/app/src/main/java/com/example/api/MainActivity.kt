package com.example.api

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var name:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        name = findViewById(R.id.nameText)
        loadUserData()
    }

    @SuppressLint("CheckResult", "SetTextI18n")
    fun loadUserData() {
        UserApi.create().getUsers()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->

                name.text = result[0].name + " " +result[0].email
                Toast.makeText(this, ""+ result[0].name + " " +result[0].email, Toast.LENGTH_SHORT).show()
//                renderCoupons(result)

            },{
                    error->
                Toast.makeText(this, ""+ error.message, Toast.LENGTH_SHORT).show()

                error.printStackTrace()
            })
    }

//    private fun renderCoupons(user: List<Testing>) {
////        if (user.isNotEmpty()){
//            Toast.makeText(this, "${user.get(0).name} ${user.get(0).email}" , Toast.LENGTH_SHORT).show()
////        }
//    }


}


}
}