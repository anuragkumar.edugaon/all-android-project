package com.example.firebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomePage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)

        val homeFragment = HomeFragment()
        val galleryFragment = GalleryFragment()
        val notificationFragment = NotificationFragment()
        val profileFragment = ProfileFragment()

        setCurrentFragment(homeFragment)

        val button = findViewById<BottomNavigationView>(R.id.buttom)
        button.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home_nav -> setCurrentFragment(homeFragment)
                R.id.gallery_nav -> setCurrentFragment(galleryFragment)
                R.id.notification_nav -> setCurrentFragment(notificationFragment)
                R.id.profile_nav -> setCurrentFragment(profileFragment)
            }
            true
        }
    }
    private fun setCurrentFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commit()
    }}
