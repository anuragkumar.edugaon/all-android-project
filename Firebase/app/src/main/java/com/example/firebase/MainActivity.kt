package com.example.firebase

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {
    val db= FirebaseFirestore.getInstance()
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val userName= findViewById<EditText>(R.id.Uname)
        val userEmail= findViewById<EditText>(R.id.uEmail)
        val userNumber= findViewById<EditText>(R.id.uNumber)
        val userAddress= findViewById<EditText>(R.id.uAddress)
        val registerBtn= findViewById<Button>(R.id.Register)

        registerBtn.setOnClickListener{
            val uName= userName.text.toString().trim()
            val uEmail= userEmail.text.toString().trim()
            val uNumber= userNumber.text.toString().trim()
            val uAddress= userAddress.text.toString().trim()

            val storeMap = hashMapOf(
                "name" to uName,
                "email" to uEmail,
                "number" to uNumber,
                "address" to uAddress
            )
            db.collection("users").add(storeMap)
                .addOnSuccessListener {
                    val sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE).edit()
                    val docId = it.id

                    sharedPreferences.putString("userDocId",docId).apply()
                    Toast.makeText(this, "Add Successful", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this,HomePage::class.java)

                    userName.text.clear()
                    userEmail.text.clear()
                    userNumber.text.clear()
                    userAddress.text.clear()

                    startActivity(intent)
                    finish()
                }

                .addOnFailureListener {
                    Toast.makeText(this,"failed", Toast.LENGTH_SHORT).show()
                }
        }



    }
    }

