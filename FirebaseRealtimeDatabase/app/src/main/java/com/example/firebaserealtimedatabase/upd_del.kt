package com.example.firebaserealtimedatabase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_upd_del.*
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.activity_welcome.email

class upd_del : AppCompatActivity() {

    private var mFirebaseDatabase: DatabaseReference? = null
        private var mFirebaseInstance: FirebaseDatabase? = null

        private var userId: String? = null

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_upd_del)

            mFirebaseInstance = FirebaseDatabase.getInstance()

            // get reference to 'users' node
            mFirebaseDatabase = mFirebaseInstance!!.getReference("users")

            val user = FirebaseAuth.getInstance().getCurrentUser()

            // add it only if it is not saved to database
            userId = user?.getUid()

        }


        private fun updateUser(name: String, email: String) {
            // updating the user via child nodes
            if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email)) {
                mFirebaseDatabase!!.child(userId!!).child("name").setValue(name)
                mFirebaseDatabase!!.child(userId!!).child("email").setValue(email)
                Toast.makeText(applicationContext, "Successfully updated user", Toast.LENGTH_SHORT).show()
            }
            else
                Toast.makeText(applicationContext, "Unable to update user", Toast.LENGTH_SHORT).show()

        }

        fun onUpdateClicked(view: View) {
            val name = name.getText().toString()
            val email = email.getText().toString()

            updateUser(name, email)
        }

        fun onDeleteClicked(view: View) {
            mFirebaseDatabase!!.child(userId!!).removeValue()
            Toast.makeText(applicationContext, "Successfully deleted user", Toast.LENGTH_SHORT).show()

            // clear information
            txt_user.setText("")
            email.setText("")
            name.setText("")
        }

        companion object {
            private val TAG = upd_del::class.java.getSimpleName()
        }
    }
