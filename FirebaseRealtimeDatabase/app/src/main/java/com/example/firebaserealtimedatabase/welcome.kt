package com.example.firebaserealtimedatabase

import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.constraintlayout.solver.widgets.Snapshot
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.email
import kotlinx.android.synthetic.main.activity_main.username
import kotlinx.android.synthetic.main.activity_welcome.*

class welcome : AppCompatActivity() {

    private var mFirebaseDatabase: DatabaseReference?=null
    private var mFirebaseInstance: FirebaseDatabase?=null

    var userId:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        mFirebaseInstance= FirebaseDatabase.getInstance()

        //get reference to 'users' node
        mFirebaseDatabase=mFirebaseInstance!!.getReference("users")

        val user=FirebaseAuth.getInstance().currentUser

        //add it only if it is not saved to database
        if (user != null) {
            userId=user.uid
        }
        addUserChangeListener()
    }
    private fun addUserChangeListener(){
        
        //User data change Listener
        mFirebaseDatabase!!.child(userId!!).addValueEventListener(object: ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot){
                val user=dataSnapshot.getValue(User::class.java)

                //Check for null
                if(user==null){
                    Log.e(TAG,"User data is null")
                    return
                }
                Log.e(TAG,"User data is changed!"+user.name+","+user.email)

                // Display newly updated name and email
                txt_user.setText(user.name + ", " + user.email)

                //Display newly updated nme and email
                email.setText(user.email)
                username.setText(user.name)


            }
            override fun onCancelled(error: DatabaseError){
                //Failed to read value
                Log.e(TAG,"Failed to read user",error.toException())
            }
        })
    }
    fun onLogout(view: View) {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, MainActivity::class.java))
    }
    companion object{
        private val TAG=upd_del::class.java.simpleName
    }

    fun onUpdateClicked(view: View) {

        startActivity(Intent(this, upd_del::class.java))
    }
}
