package com.example.imobilepay

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class OtpAuthActivity : AppCompatActivity() {
    //variable for auth
    private lateinit var auth: FirebaseAuth
    //variable for our text input
    //field for phone and otp
    private lateinit var phoneEditText: TextInputEditText
    private lateinit var otpEditText: TextInputEditText

    //button for generating OTP and verifying OTP
    private lateinit var sendOTP:Button
    private lateinit var verifyOTP:Button

    private var storeVerification = ""

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_auth)

        auth=FirebaseAuth.getInstance()

        phoneEditText = findViewById(R.id.enter_number)
        otpEditText = findViewById(R.id.otp)

        sendOTP = findViewById(R.id.sendOtp)
        verifyOTP = findViewById(R.id.verifyBtn)

        sendOTP.setOnClickListener {
            val phone = "+91" + phoneEditText.text.toString()
            val phoneAuth = PhoneAuthOptions.newBuilder(auth)
                .setPhoneNumber(phone)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks( object : PhoneAuthProvider.OnVerificationStateChangedCallbacks(){
                    override fun onCodeSent(verificationId:String,p1:PhoneAuthProvider.ForceResendingToken) {
                       // super.onCodeSent(p0, p1)
                        Toast.makeText(this@OtpAuthActivity, "Otp Sent", Toast.LENGTH_SHORT).show()
                        storeVerification = verificationId
                    }

                    override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                        Toast.makeText(this@OtpAuthActivity, "Otp completed", Toast.LENGTH_SHORT).show()

                    }

                    override fun onVerificationFailed(p0: FirebaseException) {
                        Toast.makeText(this@OtpAuthActivity, "Otp" + p0.message, Toast.LENGTH_SHORT).show()

                    }
                })
                .build()
            PhoneAuthProvider.verifyPhoneNumber(phoneAuth)

        }
        verifyOTP.setOnClickListener {
            if ((phoneEditText.text?.isNotEmpty() ?: otpEditText.text?.isNotEmpty()) == true) {
            val otpText = otpEditText.text.toString()
            val credential = PhoneAuthProvider.getCredential(storeVerification , otpText)

            auth.signInWithCredential(credential)
                .addOnSuccessListener {
                    Toast.makeText(this, "Verify Success", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(this, "Verify Failed", Toast.LENGTH_SHORT).show()
                }
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
            else {
                Toast.makeText(this, "Fill the number and Otp ", Toast.LENGTH_SHORT).show()
            }
    }
}}