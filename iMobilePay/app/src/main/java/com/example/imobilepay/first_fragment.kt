package com.example.imobilepay

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import com.example.basicfirebase.CustomListAdapter
import com.example.basicfirebase.CustomListModel
import com.google.firebase.firestore.FirebaseFirestore


class first_fragment : Fragment() {
    //firebase FireStore is Started
    var db = FirebaseFirestore.getInstance()
    @SuppressLint("SetTextI18n", "MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_first_fragment, container, false)

        db.collection("custom_list").get()
            .addOnSuccessListener {
                    documents->
                val listView = rootView.findViewById<ListView>(R.id.customList)
                val arrayData:ArrayList<CustomListModel> = documents.toObjects(CustomListModel::class.java)as ArrayList<CustomListModel>

                listView.adapter =  activity?.let { CustomListAdapter(arrayData, it) }
            }

//        val TextView = rootView.findViewById<TextView>(R.id.ankitp)
//        db.collection("users").get().addOnSuccessListener {
//            val document = it.documents[0]
//            var id = document.data?.get("id")
//            var name = document.data?.get("name")
//            var email = document.data?.get("email")
//            var number = document.data?.get("number")
//
//        }
        return rootView
    }

}