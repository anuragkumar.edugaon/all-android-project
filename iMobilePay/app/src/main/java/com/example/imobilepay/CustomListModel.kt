package com.example.basicfirebase

data class CustomListModel(
    val title:String?=null,
    val description:String?=null,
    val imageUrl:String?=null
)
