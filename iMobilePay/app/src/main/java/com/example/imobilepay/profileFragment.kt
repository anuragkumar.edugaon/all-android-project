package com.example.imobilepay

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class profileFragment : Fragment() {
    //firebase FireStore is Started
    var fireBaseFireStore= FirebaseFirestore.getInstance()

    lateinit var auth:FirebaseAuth

    @SuppressLint("MissingInflatedId", "SetTextI18n", "CommitPrefEdits", "SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)

        val TextView =rootView.findViewById<TextView>(R.id.ankit)

        auth=FirebaseAuth.getInstance()

        val sharePreferences= activity?.getSharedPreferences("register", Context.MODE_PRIVATE)
        val docid=sharePreferences?.getString("UserId","")
        val editPreference= sharePreferences?.edit()

        //firebase get data code start


        auth.currentUser?.uid?.let {
            fireBaseFireStore.collection("users").document(it).get()
                .addOnSuccessListener{

                    val name = it.data?.get("name")
                    val email = it.data?.get("email")
                    val password = it.data?.get("password")


                TextView.text="Name:-"+name.toString()+"\n"+"Email:-"+email.toString()+"\n"+"password:-"+password.toString()

            }
        }

       val logout=rootView.findViewById<Button>(R.id.logout3)
        logout.setOnClickListener{
            auth.signOut()

            val intent = Intent(activity, LoginActivity::class.java)
                startActivity(intent)
                activity?.finish()
            Toast.makeText(activity, "Logout Successful", Toast.LENGTH_SHORT).show()
//                val loginSharePreferences= activity?.getSharedPreferences("Login", Context.MODE_PRIVATE)
//                loginSharePreferences?.edit()?.putBoolean("LoginKey", false)?.commit()

            }


        return rootView;
    }
}