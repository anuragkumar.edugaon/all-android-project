package com.example.googlelogin

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.hbb20.CountryCodePicker

class PhoneAuth : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    val requestCode:Int=123


    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.phoneauth)

        auth=FirebaseAuth.getInstance()

        // Configure Google Sign In inside onCreate method
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        // getting the value of gso inside the GoogleSignInClient
        val googleSignInClient = GoogleSignIn.getClient(this,gso)
        val googleAuth =findViewById<TextView>(R.id.google)
        googleAuth.setOnClickListener{
            val googleIntent = googleSignInClient.signInIntent
            startActivityForResult(googleIntent,requestCode)
        }


        val phoneNumber = findViewById<TextInputEditText>(R.id.phoneNumber)
        val ccp = findViewById<CountryCodePicker>(R.id.ccp)

        ccp.registerCarrierNumberEditText(phoneNumber)

        val sendOtp = findViewById<AppCompatButton>(R.id.sendOtp)
        sendOtp.setOnClickListener {
            val intent = Intent(this,EnterOtp::class.java)
            intent.putExtra("phoneNumber",ccp.fullNumberWithPlus.replace(" ",""))
            startActivity(intent)
            finish()
        }
    }
    override fun onActivityResult(activityRequestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(activityRequestCode, resultCode, data)

        if (activityRequestCode == requestCode) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            task.addOnSuccessListener { it ->
                val credencial = GoogleAuthProvider.getCredential(it.idToken, null)
                auth.signInWithCredential(credencial)
                    .addOnSuccessListener {
                        startActivity(Intent(this, Profile::class.java))
                        finish()
                        Toast.makeText(this, "" + it.user?.displayName, Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()
                    }
            }
                .addOnFailureListener {
                    Toast.makeText(this, "" + it.message, Toast.LENGTH_SHORT).show()

                }
        }
    }

}