package com.example.googlelogin

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class EnterOtp : AppCompatActivity() {
    lateinit var auth:FirebaseAuth
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_otp)

        auth=FirebaseAuth.getInstance()
        val getPhoneNumber = intent.getStringExtra("phoneNumber")
        val enterOtp=findViewById<TextInputEditText>(R.id.enterOtp)

        val verifyOTP = findViewById<AppCompatButton>(R.id.verifyOtp)
        verifyOTP.setOnClickListener {
            if (enterOtp.text?.isNotEmpty() == true) {
                val credential = PhoneAuthProvider.getCredential(
                    GlobalVeriable.id.toString(),
                    enterOtp.text.toString()
                )
                auth.signInWithCredential(credential)
                    .addOnSuccessListener {
                        val intent = Intent(this, Profile::class.java)
                        startActivity(intent)
                        finish()
                    }
            }
            else {
                Toast.makeText(this, "please fill otp", Toast.LENGTH_SHORT).show()
            }
        }
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(getPhoneNumber.toString())       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }
}